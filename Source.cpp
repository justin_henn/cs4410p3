// Justin Henn
// cs4410 hw3
// 11/28/2017

#include <gl/glut.h>  
#include <stdio.h>
#include "RGBpixmap.h"
#include <string.h>
#include <stdlib.h>
#include <math.h>

//GLUquadricObj *obj = gluNewQuadric();
int rot = 0, rot1 = 0;
int num_rot[] = { 0, 0, 0 };
int num_rot1[] = { 0, 0, 0 };
double trans_x[] = { 0.0, 0.0, 0.0 };
double trans_y[] = { -0.3, -0.3, -0.3 };
double trans_z[] = { 0.1, 0.1, 0.1 };
float change_x[] = { 0.0, 0.0, 0.0 };
float change_y[] = { 0.0, 0.0, 0.0 };
float change_z[] = { 0.0, 0.0, 0.0 };
RGBpixmap pix[1];
int hit_detect[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ,1 ,1 };
int num_balls[] = { 0, 0, 0 };
//float matrix[16];
int new_game, should_exit;


//used to pick up keyboard arrows
static void SpecialKey(int key, int x, int y)
{
	switch (key) {
	case GLUT_KEY_UP:
		if (rot > -10)
			rot--;
		break;
	case GLUT_KEY_DOWN:
		if (rot < 120)
			rot++;
		/*glColor3f(0.0f, 0.0f, 0.0f);
		glPushMatrix();
		glTranslated(0.0, -0.3, 0.1);
		glRotated(rot, 0, 1, 0);
		//glRotated(45, 1, 1, 0);
		gluCylinder(obj, 0.03, 0.07, 1.5, 30, 30);
		glPopMatrix();*/
		break;
	case GLUT_KEY_LEFT:
		if (rot1 > -45)
			rot1--;
		break;
	case GLUT_KEY_RIGHT:
		if (rot1 < 45)
			rot1++;
		break;
	}
}


//pick up keyboard presses 

void myKeyboard(unsigned char key, int x, int y) {

	//printf("%d \n", key);
	switch (key) {
	/*case 's':
		if (rot < 45)
			rot++;
		glColor3f(0.0f, 0.0f, 0.0f);
		glPushMatrix();
		glTranslated(0.0, -0.3, 0.1);
		glRotated(rot, 0, 1, 0);
		//glRotated(45, 1, 1, 0);
		gluCylinder(obj, 0.03, 0.07, 1.5, 30, 30);
		glPopMatrix();
		break;

	case 'd':
		if (rot1 < 30)
			rot1++;
		break;

	case 'w':
		if (rot > -10)
			rot--;
		break;*/

	case 32:

		for (int x = 0; x < 3; x++) {

			if (num_balls[x] == 0) {

				num_balls[x] = 1;
				//printf("%d", num_balls[x]);
				break;
			}
		}
		/*for (int x = 0; x < 16; x++)
		printf("%f \n", matrix[x]);
		printf("\n");*/
		break;

	case 'y':

		new_game = 0;
		for (int x = 0; x < 18; x++) {

			if (hit_detect[x] == 1) {

				new_game = 1;
				break;
			}
		}
		if (new_game == 0) {

			for (int x = 0; x < 18; x++)
				hit_detect[x] = 1;
		}
		break;

	case 'n':

		should_exit = 0;
		for (int x = 0; x < 18; x++) {

			if (hit_detect[x] == 1) {

				should_exit = 1;
				break;
			}
		}

		if (should_exit == 0)
			exit(0);
		break;



	case 'q':
		exit(0);

	}



	//glFlush();
	//glutSwapBuffers();
	//glutPostRedisplay();

}

void display(void) {

	//material lighting

	GLfloat mat_ambient[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	GLfloat mat_diffuse[] = { 1.0f, 1.0f, 0.3f, 1.0f };
	GLfloat mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat mat_shininess[] = { 50.0f };
	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);


	//lighting

	GLfloat lightIntensity[] = { 0.7f, 0.7f, 0.7f, 1.0f };
	GLfloat light_position[] = { -1.5f, 4.0f, 2.0f, 1.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightIntensity);

	//projection

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//glFrustum(-.5, .5, -winHt*factor, winHt*factor, 1, 100.0);

	gluPerspective(45, 1.0, 0.5, 100.0);

	//modelview

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();
	gluLookAt(0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	mat_diffuse[0] = 1.0f, mat_diffuse[1] = 1.0f, mat_diffuse[2] = 1.0f, mat_diffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glColorMaterial(GL_FRONT, GL_DIFFUSE);
	glEnable(GL_COLOR_MATERIAL);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);



	//variables for rotation and translation

	static double rotateBy = 0;
	rotateBy += .03;
	static double translate_by[] = { -0.4, -0.2, 0.0, 0.2, 0.4, 0.6 };
	static double translate_by_middle[] = { -0.6, -0.4, -0.2, 0.0, 0.2, 0.4 };

	for (int y = 0; y < 6; y++) {

		translate_by[y] += .0001;
		if (translate_by[y] >= 0.6)
			translate_by[y] = -0.6;
	}
	for (int y = 0; y < 6; y++) {

		translate_by_middle[y] -= .0001;
		if (translate_by_middle[y] <= -0.6)
			translate_by_middle[y] = 0.6;
	}

	//set shading and brown color

	glShadeModel(GL_SMOOTH);
	glColor3f(0.5f, 0.35f, 0.05f);


	//glShadeModel(GL_FLAT);


	//setting texture and creating queads for background

	glBindTexture(GL_TEXTURE_2D, 2001);

	glEnable(GL_TEXTURE_2D);
	glBegin(GL_QUADS);
	glTexCoord2f(0.0, 0.0); glVertex3f(-.5, -.5, -.5);
	glTexCoord2f(0.0, 2.0); glVertex3f(.5, -.5, -.5);
	glTexCoord2f(2.0, 2.0); glVertex3f(.5, -.5, .5);
	glTexCoord2f(2.0, 0.0); glVertex3f(-.5, -.5, .5);

	glTexCoord2f(0.0, 0.0); glVertex3f(-.5, .5, -.5);
	glTexCoord2f(0.0, 2.0); glVertex3f(.5, .5, -.5);
	glTexCoord2f(2.0, 2.0); glVertex3f(.5, .5, .5);
	glTexCoord2f(2.0, 0.0); glVertex3f(-.5, .5, .5);

	glTexCoord2f(0.0, 0.0); glVertex3f(-.5, -.5, -.5);
	glTexCoord2f(0.0, 2.0); glVertex3f(.5, -.5, -.5);
	glTexCoord2f(2.0, 2.0); glVertex3f(.5, .5, -.5);
	glTexCoord2f(2.0, 0.0); glVertex3f(-.5, .5, -.5);

	glTexCoord2f(0.0, 0.0); glVertex3f(-.5, -.5, -.5);
	glTexCoord2f(0.0, 2.0); glVertex3f(.5, -.5, -.5);
	glTexCoord2f(2.0, 2.0); glVertex3f(.5, .5, -.5);
	glTexCoord2f(2.0, 0.0); glVertex3f(-.5, .5, -.5);

	glTexCoord2f(0.0, 0.0); glVertex3f(.5, .5, .5);
	glTexCoord2f(0.0, 2.0); glVertex3f(.5, -.5, .5);
	glTexCoord2f(2.0, 2.0); glVertex3f(.5, -.5, -.5);
	glTexCoord2f(2.0, 0.0); glVertex3f(.5, .5, -.5);

	glTexCoord2f(0.0, 0.0); glVertex3f(-.5, .5, .5);
	glTexCoord2f(0.0, 2.0); glVertex3f(-.5, -.5, .5);
	glTexCoord2f(2.0, 2.0); glVertex3f(-.5, -.5, -.5);
	glTexCoord2f(2.0, 0.0); glVertex3f(-.5, .5, -.5);
	glEnd();


	//counts how many teapots are left


	//int arg = 10;
	//string arg1 = to_string(arg);
	int total = 0;
	for (int x = 0; x < 18; x++) {
		//	if (hit_detect[x] == 1)
		//		total++;
		total = total + hit_detect[x];
	}


	//creates the string depending on if there are teapots left or not
	string test;

	if (total > 0) {

		string str = to_string(total);
		//string test = "Teapots left: 15" + total;

		test = "Teapots left: " + str;

	}

	else
		test = "Continue? (y/n)";




	/*glPushMatrix();
	glScaled(-.5, -.5, -.5);
	glutSolidCube(1.0);
	glPopMatrix();*/


	//create cubes for wall no longer used as changed wall back to quads

	/*	glPushMatrix();
	glTranslated(0.0, -.5, -.5);
	glScaled(.5, 0.0, .5);
	glutSolidCube(2.2);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.0, .5, -.5);
	glScaled(.5, 0.0, .5);
	glutSolidCube(2.2);
	glPopMatrix();

	glPushMatrix();
	glTranslated(-0.5, 0.0, -.5);
	glScaled(0.0001, 0.5, 0.5);
	glRotated(90, 0, 0, 1);
	glutSolidCube(2.2);
	glPopMatrix();

	glPushMatrix();
	//glTranslated(0.5, 0.0, -.5);
	glScaled(0.0001, 0.5, 0.5);
	//glRotated(90, 0, 0, 1);
	glutSolidCube(2.2);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.5, 0.0, -0.5);
	glScaled(0.0001, 0.5, 0.5);
	//glRotated(90, 1, 0, 0);
	glutSolidCube(2.2);
	glPopMatrix();

	glPushMatrix();
	glTranslated(0.0, 0.0, -.5);
	glScaled(0.5, 0.5, 0.0);
	glutSolidCube(2.2);
	glPopMatrix();*/

	glDisable(GL_TEXTURE_2D);


	//puts text on screen

	glColor3f(0.0f, 0.0f, 0.0f);
	glRasterPos2d(-.08, .35);
	//printf("%d", test.size());
	//int z = test.size();
	for (int y = 0; y < test.size(); y++)
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, test[y]);


	//create cylinder for cannon

	GLUquadricObj *obj = gluNewQuadric();
	glPushMatrix();
	glTranslated(0.0, -0.3, 0.1);
	glRotated(rot1, 0, 1, 0);
	glRotated(rot, 1, 0, 0);
	//glGetFloatv(GL_MODELVIEW_MATRIX, matrix);
	gluCylinder(obj, 0.03, 0.07, 1.5, 30, 30);
	glPopMatrix();


	//create teapots


	if (hit_detect[0] == 1) {
		glColor3f(0.0f, 1.0f, 1.0f);
		glPushMatrix();
		glTranslated((translate_by[0]), 0.3, -0.2);
		glRotated((2 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[1] == 1) {
		glColor3f(0.2f, 0.05f, 1.0f);
		glPushMatrix();
		glTranslated((translate_by[1]), 0.3, -0.2);
		glRotated((50 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[2] == 1) {
		glColor3f(0.25f, 0.02f, 0.44f);
		glPushMatrix();
		glTranslated(translate_by[2], 0.3, -0.2);
		glRotated((75 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[3] == 1) {
		glColor3f(0.35f, 0.12f, 0.74f);
		glPushMatrix();
		glTranslated(translate_by[3], 0.3, -0.2);
		glRotated((90 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[4] == 1) {
		glColor3f(0.75f, 0.22f, 0.11f);
		glPushMatrix();
		glTranslated(translate_by[4], 0.3, -0.2);
		glRotated((120 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[5] == 1) {
		glColor3f(0.73f, 0.32f, 0.51f);
		glPushMatrix();
		glTranslated(translate_by[5], 0.3, -0.2);
		glRotated((120 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[6] == 1) {
		glColor3f(0.93f, 0.46f, 0.31f);
		glPushMatrix();
		glTranslated(translate_by_middle[0], 0.0, -0.2);
		glRotated((120 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[7] == 1) {
		glColor3f(0.34f, 0.72f, 0.66f);
		glPushMatrix();
		glTranslated(translate_by_middle[1], 0.0, -0.2);
		glRotated((30 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[8] == 1) {
		glColor3f(0.13f, 0.37f, 0.55f);
		glPushMatrix();
		glTranslated(translate_by_middle[2], 0.0, -0.2);
		glRotated((75 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[9] == 1) {
		glColor3f(0.77f, 0.86f, 0.19f);
		glPushMatrix();
		glTranslated(translate_by_middle[3], 0.0, -0.2);
		glRotated((180 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[10] == 1) {
		glColor3f(0.23f, 0.35f, 0.68f);
		glPushMatrix();
		glTranslated(translate_by_middle[4], 0.0, -0.2);
		glRotated((45 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[11] == 1) {
		glColor3f(0.53f, 0.75f, 0.28f);
		glPushMatrix();
		glTranslated(translate_by_middle[5], 0.0, -0.2);
		glRotated((45 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[12] == 1) {
		glColor3f(0.11f, 0.88f, 0.33f);
		glPushMatrix();
		glTranslated((translate_by[0]), -0.3, -0.2);
		glRotated((30 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}
	if (hit_detect[13] == 1) {
		glColor3f(0.44f, 0.66f, 0.22f);
		glPushMatrix();
		glTranslated((translate_by[1]), -0.3, -0.2);
		glRotated((160 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[14] == 1) {
		glColor3f(0.34f, 0.97f, 0.87f);
		glPushMatrix();
		glTranslated((translate_by[2]), -0.3, -0.2);
		glRotated((45 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[15] == 1) {
		glColor3f(0.57f, 0.14f, 0.0f);
		glPushMatrix();
		glTranslated((translate_by[3]), -0.3, -0.2);
		glRotated((90 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[16] == 1) {
		glColor3f(0.76f, 0.25f, 0.54f);
		glPushMatrix();
		glTranslated((translate_by[4]), -0.3, -0.2);
		glRotated((145 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}

	if (hit_detect[17] == 1) {
		glColor3f(0.36f, 0.21f, 0.84f);
		glPushMatrix();
		glTranslated((translate_by[5]), -0.3, -0.2);
		glRotated((145 + rotateBy), 0, 1, 0);
		glutSolidTeapot(0.05);
		glPopMatrix();
	}
	/*int total_balls = 0;

	for (int x = 0; x < 3; x++) {

	if (num_balls[x] == 1) {

	total_balls++;
	}
	}

	if (total_balls > 0) {

	for (int x = 0; x < 3; x++) {

	if (num_rot[x] == 0) {
	num_rot[x] = rot;
	num_rot1[x] = rot1;
	}
	} */



	//documents the rotation angle for the new cannonball being created

	for (int x = 0; x < 3; x++) {

		if (num_balls[x] == 1) {

			if (num_rot[x] == 0) {

				num_rot[x] = rot;
				num_rot1[x] = rot1;

			}
		}

		else {

			num_rot[x] = 0;
			num_rot1[x] = 0;

		}

		//shoots cannonball at direction vector calculated  

		if (num_balls[x] == 1) {

			glColor3f(0.0f, 0.0f, 0.0f);
			glPushMatrix();
			glTranslated(trans_x[x], trans_y[x], trans_z[x]);
			glutSolidSphere(0.03, 30, 30);
			glPopMatrix();

			if (change_x[x] == 0.0 && change_y[x] == 0.0 && change_z[x] == 0.0) {
				if (num_rot[x] != 0 && num_rot1[x] != 0) {
					change_y[x] = (-0.3) - ((cos((num_rot[x] * 3.14 / 180)) * -0.3) + ((-sin((num_rot[x] * 3.14 / 180))) * (-1.4)));
					change_z[x] = 0.1 - ((sin((num_rot[x] * 3.14 / 180)) * -0.3) + (cos((num_rot[x] * 3.14 / 180)) * (-1.4)));
					change_z[x] *= -1;
					change_x[x] = 0.0 - sin((num_rot1[x] * 3.14 / 180)) * (change_z[x]);
					change_z[x] = 0.1 - cos((num_rot1[x] * 3.14 / 180)) * (change_z[x]);
				}
				else if (num_rot[x] != 0) {
					change_y[x] = (-0.3) - ((cos((num_rot[x] * 3.14 / 180)) * -0.3) + ((-sin((num_rot[x] * 3.14 / 180))) * (-1.4)));
					change_z[x] = 0.1 - ((sin((num_rot[x] * 3.14 / 180)) * -0.3) + (cos((num_rot[x] * 3.14 / 180)) * (-1.4)));
				}
				//change_z[x] *= -1;
				//printf("%d\n", num_rot1[x]);
				//if (num_rot1[x] != 0)
				else {
					change_x[x] = 0.0 - sin((num_rot1[x] * 3.14 / 180)) * (-1.4);
					change_z[x] = 0.1 - cos((num_rot1[x] * 3.14 / 180)) * (-1.4);
					//	printf("%f\n", cos((num_rot1[x]*180)));
				}
				change_y[x] *= .001;
				change_x[x] *= .001;
				change_z[x] *= .001;
				//	printf("%f\n\n", change_z[x]);
			}

			//trans_x[x] -= .0009;
			//trans_y[x] += .0000;
			//trans_z[x] -= .001;


			trans_x[x] -= change_x[x];
			trans_y[x] -= change_y[x];
			trans_z[x] -= change_z[x];
			//trans_x[x] -= .0001;
			//trans_y[x] += .0000;
			//trans_z[x] -= .0001;
		}

	}

	//gets rid of cannonball if off screen

	for (int x = 0; x < 3; x++) {

		if (abs(trans_x[x]) >= .6 || abs(trans_y[x]) >= .6 || abs(trans_z[x]) >= .6) {
			num_balls[x] = 0;
			trans_x[x] = 0.0;
			trans_y[x] = -0.3;
			trans_z[x] = 0.1;
			change_x[x] = 0.0;
			change_y[x] = 0.0;
			change_z[x] = 0.0;

		}

	}


	//collision detection loops

	for (int z = 0; z < 3; z++) {

		for (int j = 0; j < 6; j++) {

			float dx = abs(trans_x[z] - translate_by[j]);
			float dy = abs(trans_y[z] - 0.3);
			float dz = abs(trans_z[z] - (-0.2));

			float dist = sqrt(dx*dx + dy*dy + dz*dz);
			if (dist <= 0.08 && hit_detect[j] == 1) {
				hit_detect[j] = 0;
				num_balls[z] = 0;
				trans_x[z] = 0.0;
				trans_y[z] = -0.3;
				trans_z[z] = 0.1;
				change_x[z] = 0.0;
				change_y[z] = 0.0;
				change_z[z] = 0.0;
			}
		}

	}

	for (int z = 0; z < 3; z++) {

		for (int j = 0, h = 6; j < 6; j++, h++) {

			float dx = abs(trans_x[z] - translate_by_middle[j]);
			float dy = abs(trans_y[z] - 0.0);
			float dz = abs(trans_z[z] - (-0.2));

			float dist = sqrt(dx*dx + dy*dy + dz*dz);
			if (dist <= 0.08 && hit_detect[h] == 1) {
				hit_detect[h] = 0;
				num_balls[z] = 0;
				trans_x[z] = 0.0;
				trans_y[z] = -0.3;
				trans_z[z] = 0.1;
				change_x[z] = 0.0;
				change_y[z] = 0.0;
				change_z[z] = 0.0;
			}
		}

	}
	for (int z = 0; z < 3; z++) {

		for (int j = 0, h = 12; j < 6; j++, h++) {

			float dx = abs(trans_x[z] - translate_by[j]);
			float dy = abs(trans_y[z] - (-0.3));
			float dz = abs(trans_z[z] - (-0.2));

			float dist = sqrt(dx*dx + dy*dy + dz*dz);
			if (dist <= 0.08 && hit_detect[h] == 1) {
				hit_detect[h] = 0;
				num_balls[z] = 0;
				trans_x[z] = 0.0;
				trans_y[z] = -0.3;
				trans_z[z] = 0.1;
				change_x[z] = 0.0;
				change_y[z] = 0.0;
				change_z[z] = 0.0;
			}
		}

	}
	//draw and redisplay

	glFlush();
	glutSwapBuffers();
	glutPostRedisplay();
}

//main function

void main(int argc, char ** argv) {

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowSize(640, 480);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("teapots");
	glutKeyboardFunc(myKeyboard);
	glutSpecialFunc(SpecialKey);
	glutDisplayFunc(display);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glEnable(GL_TEXTURE_2D);
	pix[0].readBMPFile("download.bmp");
	pix[0].setTexture(2001);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glViewport(0, 0, 640, 480);
	glutMainLoop();
}